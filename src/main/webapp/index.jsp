<%@ page session="false" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Untitled</title>
</head>
<body bgcolor="white">
  <c:choose>
    <c:when test="${now ne null and prop ne null}">
      <h2><fmt:message key="welcome" /></h2>
      <p><fmt:message key="now" /> <fmt:formatDate type="both"
                                              dateStyle="long" timeStyle="long"
                                              value="${now}" /></p>
      <small>${prop.jreName} ${prop.jreVersion}</small>
    </c:when>
    <c:otherwise>
      <p><a href="${pageContext.servletContext.contextPath}/helloWorld"><fmt:message
              key="welcome" /></a></p>
    </c:otherwise>
  </c:choose>
</body>
</html>
