package it.unibo;

import java.io.Serializable;

import static java.lang.System.getProperty;

public class PropertyBean implements Serializable {
    protected static final long serialVersionUID = 1L;

    protected final String jreName;
    protected final String jreVersion;

    protected PropertyBean(final String jreName,
                           final String jreVersion) {
        this.jreName = jreName;
        this.jreVersion = jreVersion;
    }

    public PropertyBean() {
        this(getProperty("java.vm.name"),
             getProperty("java.version"));
    }

    public String getJreName() {
        return jreName;
    }

    public String getJreVersion() {
        return jreVersion;
    }
}
