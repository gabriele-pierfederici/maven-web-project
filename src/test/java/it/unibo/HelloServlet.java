package it.unibo;

import java.util.Date;

import org.slf4j.Logger;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.RequestDispatcher;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.slf4j.LoggerFactory.getLogger;

public class HelloServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger log = getLogger(HelloServlet.class);

    @Override
    public void init() throws ServletException {
        if (log.isDebugEnabled())
            log.debug("Servlet initialized");
    }

    @Override
    public void doGet(final HttpServletRequest request,
                      final HttpServletResponse response)
            throws ServletException, IOException {
        final String nextJsp = "/index.jsp";
        final ServletContext context = getServletContext();
        final RequestDispatcher dispatcher = context.getRequestDispatcher(nextJsp);
        request.setAttribute("now", new Date());
        request.setAttribute("prop", new PropertyBean());
        if (log.isDebugEnabled())
            log.debug("Forwarding request to {}", nextJsp);
        dispatcher.forward(request,response);
    }

    @Override
    public void destroy() {
        // do nothing.
    }
}